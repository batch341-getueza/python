class SampleClass():
	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f"The year is: {self.year}")

# Create an instance
myObj = SampleClass(2023)

print(myObj.year)
myObj.show_year()

# [SECTION] Fundamentals of OOP
# There are four main fundamental principles of OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstraction

# [SECTION] Encapsulation
# In encapsulation, the attributes of a class will be hidden from other class
# To achieve encapsulation -
# 1. Declare the attributes of a class
# 2. Proviede getter and setter methods to modify the view the attribute values

class Person():
	def __init__(self):
		# protected attribute _name
		self._name = "John Doe"
		self._age = 0

	def set_name(self, name):
		self._name = name

	def get_name(self):
		print(f"Name of person: {self._name}")

	def set_age(self, age):
		self._age = age

	def get_age(self):
		print(f"Age of person: {self._age}")

p1 = Person()
p1.get_name()
p1.set_name("Bob Doe")
p1.get_name()
print(p1._name)

# Test Cases:
p1.set_age(23)
p1.get_age()

# [SECTION] Inheritance
# To create inheritance
class Employee(Person):
	def __init__(self, employeeId):
		super().__init__()
		self._employeeId = employeeId

	def set_employeeId(self, employeeId):
		self._employeeId = employeeId

	def get_employee(self):
		print(f"The Employee ID is {self.employeeId}")

	def get_details(self):
		print(f"{self._employeeId} belong to {self._name}")

emp1 = Employee("Emp-001")
emp1.get_details()
emp1.set_name("Jane Doe")
emp1.set_age(40)
emp1.get_age()
emp1.get_details()

# Mini exercise
# 1. Create a new class called Student that inherits Person with the additional attributes and methods
# attributes: Student No, Course, Year Level
# methods: 
# get_detail: prints the output "<Student name> is currently in year <year level> taking up <Course>

class Student(Person):
	def __init__(self, student_num, course, year_level):
		super().__init__()
		self._student_num = student_num
		self._course = course
		self._year_level = year_level

	def get_details(self):
		print(f"{self._name} is currently in year {self._year_level} taking up {self._course}")

new_student = Student("std-001", "Python short course", 4)
new_student.get_details();

# [SECTION] Polymorphism

# Functions and objects
# A function can be created that can take any objet, allowing for polymorphism

class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print("Admin User")

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print("Regular User")

# Define a test_function that will take an object

def test_function(obj):
	obj.is_admin()
	obj.user_type()

# Instances for Admin and Customer
user_admin = Admin()
user_customer = Customer()

# Pass the created instances to test_function
test_function(user_admin)
test_function(user_customer)

# What happened is that the test_function would call the methods of the object passed to it, hence allowing it to have different outputs based on the objects


# Polymorphism with Class methods
class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	# access the occupation method of each item
	person.occupation()

# Polymorphism with Inheritance
class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks (developer career, pi-shape career, and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours")

class DeveloperCareer(Zuitt):
	# overrides the parent's num_of_hours(method)
	def num_of_hours(self):
		print("Learn the basics of web development in 240 hours!")

class PiShapedCareer(Zuitt):
	# overrides the parent's num_of_hours(method)
	def num_of_hours(self):
		print("Learn skills for no-code app development in 140 hours!")

class ShortCourses(Zuitt):
	# overrides the parent's num_of_hours(method)
	def num_of_hours(self):
		print("Learn advanced topics in web development in 20 hours!")

course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ShortCourses()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()