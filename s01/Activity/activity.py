# Create 5 variables and output them in the command prompt in following format:

name = "Jose"
age = 38
occupation = "writer"
movie = "One more chance"
rating = 99.6

print("I am " + name + " , and I am " + str(age) + " years old. I work as a writer , and my rating for " + movie + " is " + str(rating) + " %")

# Create 3 variable, num1, num2, and num3
num1 = 10
num2 = 15
num3 = 20

print(num1 * num2)
print(num1 < num2)
print(num3 + num2)