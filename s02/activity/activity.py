# S02 Activity:
# Get a year from the user and determine if it is a leap year or not.

# userInput = int(input("Enter a year: "))

# if userInput % 4 == 0:
# 	print(f"{userInput} is a leap year")
# else:
# 	print(f"{userInput} is not a leap year")


# Get two numbers (row and col) from the user and create by row x col grid of asterisks given the two numbers.

# userRows = int(input("Enter number of rows: "))
# userCols = int(input("Enter number of columns: "))

# for x in range(userRows):
# 	for y in range(userCols):
# 		print("*", end=" ")
# 	print()


# Stretch goal: Implement error checking for the leap year input
# Strings are not allowed for inputs
# No zero or negative values