# Python Lists, Dictionaries, Functions, and Classes

# [SECTION] Lists
# Lists are similar to JS arrays
# To create a list, the square brackets([]) are used

names = ["John", "Paul", "George", "Ringo"]
programs = ["developer carreer", "pi-shape", "short courses"]
durations = [260, 180, 20]
truth_values = [True, False, True, True, False]
print(names)
print(programs)
print(durations)
print(truth_values)

sample_list = ["Apple", 3, False, "Potato", 4, True]
print(sample_list)

# Getting the list size
# The number of elements in a list can be counted using the len() method
print(len(programs))

# Accessing values
# The index numbers in lists start at 0 and ends at (n-1), where n is the numebr of elements
# Accessing the first element is the list
print(programs[0])

# Accessing the last element in the list
print(programs[-1])

# Accessing a range of values
# list_name[start index: end index]
# Note: The end index is not included
print(programs[0:2]) # will display index 0 and 1

# [Section] Mini exercise:
    # 1. Create a list of names of 5 students
    # 2. Create a list of grades for the 5 students
    # 3. Use a loop to iterate through the lists printing in the following format:

# student_list = ["John", "Mykl", "Kira", "Carl", "Kayko"]
# grades = [89, 90, 78, 85, 95]

# counts = 0
# for counts in range(len(student_list)):
# 	counts += 1
# 	print(f"The grade of {student_list[counts]} is {grades[counts]}")

# Updating lists 
print(f"Current value: {programs[2]}")

# Update the value
programs[2] = "Short Courses"

# Print the new value
print(f"New value: {programs[2]}")

# List Manipulation
# Adding list items - append() method allows to insert items to a list
programs.append("global")
print(programs)

# Deleting List Items - the "del" keyword can be used to delete elements in the list
durations.append(360)
print(durations)

# Delete the last  item on the list
del durations[-1]
print(durations)

# Membership checks - the "in" keyword check if the element is in the list
print(20 in durations) # True
print(500 in durations) # False

# Sorting lists - the sort() method sorts the list alphanumerically, ascending by default
names.sort()
print(names)

# Emptying a list - the clear() method is used to empthy the content of a list
test_list = [1, 3, 5, 7, 9]
print(test_list)
test_list.clear()
print(test_list)


# [SECTION] Dictionaries
# Dictionaries are used to store data values in key:value pairs. This is similar to the objects in JS.

person1 = {
	"name" : "Daisy",
	"age" : 28,
	"occupation" : "instructor",
	"isEnroleld" : True,
	"subjects" : ["Python", "SQL", "Django"]
}

print(person1)
# To get the number of key-pairs in a dictionary, the len() method can be used
print(len(person1))

# Accessing values in a dictionary\
# To get the items in a dictionary, the key name can be referred using square bracket([])
print(person1["name"])

# The keys() method will return a list of all the keys in the dictionary
print(person1.keys())

# The values( method will return a list of all the values in the dictionary)
print(person1.values())

# The items() method will return each item in a dictionary, as key-value pairs in a list 
print(person1.items())

# Adding key-value pairs can be done with either putting a new key and assigning a value or the update() method
person1["nationality"] = "Filipino"
print(person1)

person1.update({"fav_food" : "Sinigang"})
print(person1)

# Deleting entries can be done using the pop() method or the del keyword
person1.pop("fav_food")
print(person1)
del person1["nationality"]
print(person1)

# The clear() method empties a dictionary
person2 = {
	"name" : "John",
	"age" : 18,
}
print(person2)
person2.clear()
print(person2)

# Looping through dictionaries
for key in person1:
	print(f"The value of {key} is {person1[key]}")

# Nested dictionaries
person3 = {
	"name" : "Monica",
	"age" : 20,
	"occupation" : "poet",
	"isEnroleld" : True,
	"subjects" : ["Python", "SQL", "Django"]
}

classRoom = {
	"student1" : person1,
	"student2" : person3
}

print(classRoom)

# [Section] Mini Exercise
# Create a car dictionary with the following keys:
# brand, model, year of make, color
# Print the following statement from the details:
# "I own a <Brand> <Model> and it was made in <Year of Make>"


car = {
	"brand" : "Toyota",
	"model" : "Tacoma",
	"yearOfMake" : "2023",
	"color" : "powderblue"
}

print(f"I own a {car['brand']} {car['model']} and it was made in {car['yearOfMake']}")


print("--------------------")
print(person1)
person1["subjects"].append("React")
print(person1)

# [SECTION] Functions
# Function are blocks of code that run when called

# "def" keyword is used to create functions
# def <function name>

def my_greeting():
	# Code to be run when my_greeting is called
	print("Hello, User!")

# Calling/Invoking a function - just specify the function anme and provide the values.
my_greeting()

# Parameters can be added to functions to have more control to what the inputs for the function will be
def greet_user(username):
	print(f"Hello, {username}")

# Arguments are the values that are provided to the function/ subtituted to the parameters
greet_user("Bob")
greet_user("Amy")

# Return statement - the "return" keyword allows function to return values
def addition(num1, num2):
	return num1 + num2

sum = addition(5, 10)
print(f"The sum is {sum}")

# [SECTION] Lambda function
# A lambda function is a small, anonymous function that can be used for callbacks

greeting = lambda person : f"Hello, {person}"
print(greeting("Elsie"))
print(greeting("Anthony"))

mult = lambda a, b : a * b 
print(mult(5, 6))
print(mult(6, 99))

# Mini exercise:
# Create a function that gets the square of a number

def square (num):
	squared = num * num
	return f"The square of {num} is {squared}"

print(square(5))

# [SECTION] Classes
# Classes serve as blueprints to describe the concept of objects
# Each object has characteristics(properties) and behaviors(methods)
# To create a class, the "class" keyword is used along with the class name that starts with an uppercase letter
class Car():
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		self.fuel = "Gasoline"
		self.fuel_level = 0

	# methods
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print("Filling up the fuel tank...")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")

	# mini acitivity
	def drive(self, distance):
		self.distance = distance
		self.fuel_level = 25
		result = self.distance - self.fuel_level
		print(f"The car has driven {self.distance} kilometers")
		print(f"The car's fuel level is {result}")

# Creating a new instance is done by call the class and providing the arguments
new_car = Car("Nissan", "GT-R", "2019")

# Displaying attributes can be done using dot notation
print(f"My car is a {new_car.brand} {new_car.model}")

# Calling methods of the instance
new_car.fill_fuel()

print("-----Mini Activity-----")
# # Mini Exercise
# 1. Add a method called drive with a parameter called distance
# 2. The method would output 2 things
# "The car has driven <distance> kilometers"
# "The car's fuel level is <fuel level - distance>"

new_car.drive(100)
